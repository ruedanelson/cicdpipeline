package com.example.presenter;

import android.content.Context;

import com.example.model.LoginInteractor;
import com.example.model.models.CorrespondentBankUser;
import com.example.model.utils.Datos;
import com.example.view.views.LoginView;

public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnLoginFinishedListener {

    private LoginView loginView;
    private LoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView, LoginInteractor loginInteractor) {
        this.loginView = loginView;
        this.loginInteractor = loginInteractor;
    }

    @Override
    public void validateCredentials(String username, String password, Context context) {

        if (loginView != null) {
            loginView.showProgress();
        }

        loginInteractor.login(username, password, this, context);

    }


    @Override
    public void onDestroy() {

        loginView = null;

    }

    @Override
    public void onUsernameError() {

        if (loginView != null) {
            loginView.setUserNameError();
            loginView.hideProgress();
        }

    }

    @Override
    public void onPasswordError() {

        if (loginView != null) {
            loginView.setPasswordError();
            loginView.hideProgress();
        }

    }

    @Override
    public void onSuccess() {

        if (loginView != null) {
            loginView.navigateToHome();
            loginView.hideProgress();
        }

    }
}
